import os, sys, traceback, time

from concerto.assembly import Assembly
import diet_client
from naming_service import NamingService
from master_agent import MasterAgent
from local_agent import LocalAgent
from server_daemon import ServerDaemon

# NEED TO UPDATE THIS CLASS
	# need to add a function to connect/reconnect new/old components

	# need to add a function (dynamic_reconfiguration) to dynamicaly reconfigure diet
	# FR : pour les reconfigurations : si un composant n'a pas de parent, il reste dans l'état déconnecté.
	# FR : si un composant n'existe plus dans le fichier de reconfiguration, il est éteint/kill
	# vérifier ce qu'il se passe lorsqu'un dépendance n'est plus remplie? (est-ce que le push_b est toujours en fonction si la dépendance et re-rempli plus tard ? est-ce que concerto utilise une transitions pour revenir à l'état précédent ?)


class TestDietAssembly(Assembly):
	def __init__(self):
		self.NS_name = 'Naming Service'
		self.MA_name = 'MA1'
		self.LA1_name = 'LA1'
		self.LA2_name = 'LA2'
		self.SED1_name = 'SeD1'
		self.SED2_name = 'SeD2'
		self.hostname = 'localhost'
		Assembly.__init__(self)

	def diet_concerto_disconnect(self, agent_name):
		print("yes")
	
	def diet_concerto_connect(self, parent_name, agent_name):
		print("yes")
		self.connect(parent_name, parent_port,
						agent_name, agent_port)
		self.push_b(agent_name, 'deploy') # repush the agent in deployment

	def diet_reconfigure(self):
		print("yolo")

	def deploy(self):
		print("Adding NS componant")
		
		self.add_component(self.NS_name, NamingService(self.NS_name, self.hostname))

		print("Adding then connecting " + self.MA_name + " componant to " + self.NS_name)
		self.add_component(self.MA_name, MasterAgent(self.MA_name, self.hostname))
		self.connect(self.MA_name,'u_NS_service',
						self.NS_name,'p_NS_service')
		self.connect(self.MA_name,'du_NS_hostname',
						self.NS_name,'dp_NS_hostname')

		print("Adding then connecting " + self.LA1_name + " componant to " + self.MA_name)
		self.add_component(self.LA1_name, LocalAgent(self.LA1_name, self.hostname))
		self.connect(self.LA1_name,'du_NS_hostname',
						self.NS_name,'dp_NS_hostname')
		self.connect(self.LA1_name,'u_NS_service',
						self.NS_name,'p_NS_service')
		self.connect(self.LA1_name,'du_parent_name',
						self.MA_name,'dp_MA_name')
		self.connect(self.LA1_name,'u_parent_service',
						self.MA_name,'p_MA_service')

		print("Adding then connecting " + self.LA2_name + " componant to " + self.LA1_name)
		self.add_component(self.LA2_name, LocalAgent(self.LA2_name, self.hostname))
		self.connect(self.LA2_name,'du_NS_hostname',
						self.NS_name,'dp_NS_hostname')
		self.connect(self.LA2_name,'u_NS_service',
						self.NS_name,'p_NS_service')
		self.connect(self.LA2_name,'du_parent_name',
						self.LA1_name,'dp_LA_name')
		self.connect(self.LA2_name,'u_parent_service',
						self.LA1_name,'p_LA_service')

		print("Adding then connecting " + self.SED1_name + " componant to " + self.LA1_name)
		self.add_component(self.SED1_name, ServerDaemon(self.SED1_name, self.hostname))
		self.connect(self.SED1_name,'du_NS_hostname',
						self.NS_name,'dp_NS_hostname')
		self.connect(self.SED1_name,'u_NS_service',
						self.NS_name,'p_NS_service')
		self.connect(self.SED1_name,'du_parent_name',
						self.LA1_name,'dp_LA_name')
		self.connect(self.SED1_name,'u_parent_service',
						self.LA1_name,'p_LA_service')

		print("Adding then connecting " + self.SED2_name + " componant to " + self.LA2_name)
		self.add_component(self.SED2_name, ServerDaemon(self.SED2_name, self.hostname))
		self.connect(self.SED2_name,'du_NS_hostname',
						self.NS_name,'dp_NS_hostname')
		self.connect(self.SED2_name,'u_NS_service',
						self.NS_name,'p_NS_service')
		self.connect(self.SED2_name,'du_parent_name',
						self.LA2_name,'dp_LA_name')
		self.connect(self.SED2_name,'u_parent_service',
						self.LA2_name,'p_LA_service')
		
		# Push_b
		print("Pushing behavior 'deploy' ")
		self.push_b(self.NS_name,'deploy')
		self.push_b(self.MA_name,'deploy')
		self.push_b(self.LA1_name,'deploy')
		self.push_b(self.LA2_name,'deploy')
		self.push_b(self.SED1_name,'deploy')
		self.push_b(self.SED2_name,'deploy')

		self.wait_all()
		self.synchronize()


	def suspend(self):
		self.wait_all()
		self.synchronize()


	def restart(self):
		self.wait_all()
		self.synchronize()
	













# # connect the SeD to the MA
# print("Connection of the SeD to the MA")
# # self.push_b(self.SED1_name,'disconnect')
# self.connect(self.SED1_name,'du_parent_name',
#				 'MA','dp_self.MA_name')
# self.connect(self.SED1_name,'u_parent_service',
#				 'MA','p_MA_service')
# # self.push_b(self.SED1_name,'deploy')





