from gen_assembly_diet import TestDietAssembly

import time


# test1 = DietGenAssembly() # this need some more dependencies


test = TestDietAssembly()

test.deploy()

print("Deployed!")
time.sleep(1)
print("-- Waiting a little before reconfiguration")
time.sleep(5)

print("-- Disconnecting " + test.SED1_name + ", " + test.SED2_name + ", " + test.LA1_name + ", " + test.LA2_name + ", ")
test.push_b(test.SED1_name,'disconnect')
test.push_b(test.SED2_name,'disconnect')
test.push_b(test.LA1_name,'disconnect')
test.push_b(test.LA2_name,'disconnect')


print("-- Disconnecting concerto's port")

test.disconnect(test.SED1_name,'du_parent_name',
					test.LA1_name,'dp_LA_name')
test.disconnect(test.SED1_name,'u_parent_service',
					test.LA1_name,'p_LA_service')


test.disconnect(test.LA2_name,'du_parent_name',
					test.LA1_name,'dp_LA_name')
test.disconnect(test.LA2_name,'u_parent_service',
					test.LA1_name,'p_LA_service')

test.disconnect(test.LA1_name,'du_parent_name',
					test.MA_name,'dp_MA_name')
test.disconnect(test.LA1_name,'u_parent_service',
					test.MA_name,'p_MA_service')


print("-- Reconnecting correct port in concerto")

test.connect(test.SED1_name,'du_parent_name',
				test.MA_name,'dp_MA_name')
test.connect(test.SED1_name,'u_parent_service',
				test.MA_name,'p_MA_service')


test.connect(test.LA2_name,'du_parent_name',
				test.MA_name,'dp_MA_name')
test.connect(test.LA2_name,'u_parent_service',
				test.MA_name,'p_MA_service')

print("-- Re-pushing behavior 'deploy' on " + test.SED1_name + ", " + test.SED2_name + ", " + test.LA2_name)
test.push_b(test.SED1_name,'deploy')
test.push_b(test.SED2_name,'deploy')
test.push_b(test.LA2_name,'deploy')

print("-- Killing" , test.LA1_name , "process.")
test.push_b(test.LA1_name,'stop')


test.terminate()
time.sleep(1) # to make sure the output is before the prompt's return