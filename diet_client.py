import time

from concerto.all import *
from examples.utils import *


class DietClient(Component):

	def create(self):
		self.places = [
			'installed',
			'configured',
			'running'
		]

		self.transitions = {
			'configure': ('installed', 'configured', 'deploy', 0, self.configure),
			'run': ('configured', 'running', 'deploy', 0, self.run),
			'shutdown': ('running', 'configured', 'stop', 0, self.shutdown)
		}

		self.dependencies = {
			'du_MA_name': (DepType.DATA_USE, ['configure']), # need the name of the MA
			'du_NS_hostname': (DepType.DATA_USE, ['run']), # need the IP of the NS
			'u_service': (DepType.USE, ['running']) # need a SeD running
		}

		self.initial_place = 'installed'

	def __init__(self):
		self.du_NS_hostname = None
		Component.__init__(self)

	def configure(self):
		self.print_color("conf")

	def run(self):
		self.print_color("run")

	def shutdown(self):
		self.print_color("shutdown")
