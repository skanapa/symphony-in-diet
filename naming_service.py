import time
import os

import subprocess
import shlex

from concerto.all import *
from examples.utils import *

from basic_diet_component import BasicDietComponent

from orchestrapi.orchestrapi import OrchestrAPI

class NamingService(BasicDietComponent):

	def create(self):
		self.places = [
			'installed',
			'configured',
			'running'
		]

		self.transitions = {
			'configure': ('installed', 'configured', 'deploy', 0, self.configure),
			'run': ('configured', 'running', 'deploy', 0, self.run),
			'shutdown': ('running', 'configured', 'stop', 0, self.shutdown)
		}

		self.dependencies = {
			'dp_NS_hostname' : (DepType.DATA_PROVIDE, ['configured','running']), # the hostname or IP
			'p_NS_service' : (DepType.PROVIDE, ['running']) # provide the state of the service
		}

		self.initial_place = 'installed'

	def __init__(self, name, hostname, verbose=0):
		super().__init__(name, hostname, 0, verbose) # omniNames has no trace level
		if self.verbose > 4:
			print("NS init")

		self.api = OrchestrAPI(self.name, self.working_path, "concerto-diet" , self.hostname)
		self.path = self.api.get_path()
		self.cmd = "/usr/bin/omniNames -start -always"


	def configure(self):
		self.print_color("CONFIGURATION")
		self.write('dp_NS_hostname',self.hostname)
		self.api.transfer_files(self.path + '/',self.path)

	def run(self):
		self.print_color("STARTING")

		scriptpath = self.api.write_script(self.diet_lib, self.cmd)
		self.api.transfer_files(self.path + '/',self.path)

		self.process = self.api.run_cmd("/bin/bash " + scriptpath)

		time.sleep(2)


	def shutdown(self):
		self.print_color("SHUTTING DOWN")
		self.api.kill_scripted_cmd()
		self.process.kill()
